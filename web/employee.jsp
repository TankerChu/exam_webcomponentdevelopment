<%-- 
    Document   : employee
    Created on : Jan 8, 2020, 8:46:36 AM
    Author     : HP
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Employee</title>
    </head>
    <body>
         <form action="EmployeeServlet" method="POST">
            Employee ID:<input type="text" name="id" value="${employee.id}"/><br><br/>
            Full name: <input type="text" name="fullname" value="${employee.fullname}"/><br/><br/>
            Birthday: <input type="text" name="birthday" value="${employee.birthday}"/><br/><br/>
            Address: <input type="text" name="address" value="${employee.address}"/><br/><br/>
            Position: <input type="text" name="position" value="${employee.position}"/><br/><br/>
            Department: <input type="text" name="department" value="${employee.department}"/><br/><br/>
            
            <input type="submit" name="action" value="SUBMIT"/> |
            <input type="reset" name="action" value="RESET"/> 
        </form>
    </body>
</html>
